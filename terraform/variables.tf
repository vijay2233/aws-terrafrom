variable "access_ip" {
  default = "10.0.1.0/24"
}

variable "aws_region" {
  default = "us-east-1"
}